// --------- AXIOS API REQUEST ------------ //

// OMDB axios API request that receives the value of an input "searchTerm" to pass it as second of the request params
const fetchData = async (searchTerm) => {
	const response = await axios.get('http://www.omdbapi.com/', {
		params: {
			apikey: '16060606',
			s: searchTerm
		}
	});

	console.log(response.data);
};


// --------- MANUAL AXIOS REQUEST DEBOUNCE VERSION ------------ //

// created input selection + input eventListener connection
const input = document.querySelector('input');

// created timeoutId is set just to create a variable that further on setTimeout ID created every time setTimeout is set can be grabbed and passed to the setTimeout clear function
let timeoutId;

// code extracted from eventListener for the purpose of more clear setTimeout setup
const onInput = (event) => {

	// if event.target.value contained in timeoutId will change, then clear API request setTimeout contained in timeoutId method and set again the timer of 1000 miliseconds
	if (timeoutId) {
		clearTimeout(timeoutId);
	}

	// setTimeout has been assigned to const to catch a setTimeout ID and invoke it above in clearTimeout function
	timeoutId = setTimeout(() => {
		fetchData(event.target.value);
	}, 1000);
};

// when input event occures, call fetchData with value of an input
input.addEventListener('input', onInput);



// --------- FUNCTION AXIOS REQUEST DEBOUNCE VERSION ------------ //

// debounce (reusable) helper function that wraps in this case onInput function and by set delay value, is a doorkeeper how often onInput function can be called; it accepts two agruments that in this case is the onInput function and second is the delay that protects function against too frequent call
const debounce = (func, delay = 1000) => {
	let timeoutId;
	//
	return (...args) => {
		if (timeoutId) {
			clearTimeout(timeoutId);
		}
		timeoutId = setTimeout((event) => {
			// "apply" method allowes invoking the all function arguments
			func.apply(null, args);
		}, delay);
	};
};
// onInput function that catch an input value and invoke fetchData axios request
const onInput = (event) => {
	fetchData(event.target.value);
};
// the second eventListener agrument is debounce function with it's arguments (invoked function name, requested delay in miliseconds)
input.addEventListener('input', debounce(onInput, 500));





// ----- SEARCH INPUT INCL. DROPDOWNLIST AND SEARCHED MOVIE HTML CODE DISPLAY ---------------- //


//as we would like to create the HTML components dynamicly to make it reusable in the future, we just wrap the only created in HTML file div and fill it with the components created with innerHTML method below
const root = document.querySelector('.autocomplete');
root.innerHTML = `
  <label><b>Search For a Movie</b></label>
  <input class="input" />
  <div class="dropdown">
    <div class="dropdown-menu">
      <div class="dropdown-content results"></div>
    </div>
  </div>
`;

// below we wrap the created in the lines above HTML elements into variables
const input = document.querySelector('input');
const dropdown = document.querySelector('.dropdown');
const resultsWrapper = document.querySelector('.results');

const onInput = async (event) => {
	const movies = await fetchData(event.target.value);

	//if there are no movies related to the search term found (there is no movies.length), app removes "is-active" part of the dropdown class closing the dropdownList
	if (!movies.length) {
		dropdown.classList.remove('is-active');
		return;
	}

	// the line below is responsinble for cleaning the resukts list before the next result will be fetched
	resultsWrapper.innerHTML = '';

	// reflecting the BULMA library 'is-active' class needs to be added to the 'dropdown' class to make the dropdownlist visable and display the looped below results of the data received from an API
	dropdown.classList.add('is-active');
	for (let movie of movies) {
		// reflecting the BULMA library requirements, insted of DIVs we create <a> and call it however we would like to (in this case "option" as we will display a bunch of films to choose from the dropdown list)
		const option = document.createElement('a');

		// as not every received API result contains Poster URL, in case the poster URL result is 'N/A', the turnary expression below pass the empty string avoiding the situation that 'N/A' is passed as the Poster URL and there appears a bug in Poster display
		const imgSrc = movie.Poster === 'N/A' ? '' : movie.Poster;

		option.classList.add('dropdown-item');
		option.innerHTML = `
          <img src="${imgSrc}" />
          ${movie.Title}
        `;

		// as "option" is every particular element on dropdown list (every created by the loop <a>), we add an eventListener to be able to detect any click on the dropdown menu
		option.addEventListener('click', () => {
			// when eventListener detects a click on any "option element" that is an element of the dropdownList, it removes "is-active" className part resulting dropdownList closure
			dropdown.classList.remove('is-active');
			// also as the eventListener detects which "option" element of the dropdownList has been clicked, it gets the clicked item (movie) title and sets it in an input element
			input.value = movie.Title;
			// when user clicks on selected "option" (dropdownList element), it invokes three actions stored in the onMovieSelect metod that due to the lots of data, is exported to the separate file. The actions stored in an onMovieSelect method are: 1. do another request, 2.gets delivered by an API clicked movie detailed data, 3. renders delivered by an API detailed movie data
			onMovieSelect(movie);
		});

		// as the querySelector has been cought in const named resultsWrapper, we can use it in order to append the created anchors
		resultsWrapper.appendChild(option);
	}
};
input.addEventListener('input', debounce(onInput, 500));

// the eventListener has been added in order to check whether user made a click event on any window element outside of the root element and its scope (!root.contains(event.target)) and if so, the 'is-active' part of the class is removed from the dropdown class resulting dropdown list closure
document.addEventListener('click', (event) => {
	if (!root.contains(event.target)) {
		dropdown.classList.remove('is-active');
	}
});

// exported onMovieSelect method is responsible for making the follow-up API request on the selected "option" (movie) details and accepts the movie argument that is a set of the general information on the selected movie received from an API
const onMovieSelect = async (movie) => {
	const response = await axios.get('http://www.omdbapi.com/', {
		params: {
			apikey: '16060606',
			i: movie.imdbID
		}
	});
	// we select created in HTML file #summary <div> and fill it in with the ".innerHTML" method for the purpose of proper display of the received from an API on selected movie data following the enclosed in a movieTemplate function invoked data structure
	document.querySelector('#summary').innerHTML = movieTemplate(response.data);
};

// the movieTemplate function is responsible for display of the selected movie detailed data and accepts the movieDetail argument that is an object containing set of the selected movie details information received from the API request executed by the onMovieSelect function set above
const movieTemplate = (movieDetail) => {
	// the movieDetail function returns a set of HTML code related to the BULMA library to show the selected movie details in the app
	return `
    <article class="media">
      <figure class="media-left">
        <p class="image">
          <img src="${movieDetail.Poster}" />
        </p>
      </figure>
      <div class="media-content">
        <div class="content">
          <h1>${movieDetail.Title}</h1>
          <h4>${movieDetail.Genre}</h4>
          <p>${movieDetail.Plot}</p>
        </div>
      </div>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.Awards}</p>
      <p class="subtitle">Awards</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.BoxOffice}</p>
      <p class="subtitle">Box Office</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.Metascore}</p>
      <p class="subtitle">Metascore</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.imdbRating}</p>
      <p class="subtitle">IMDB Rating</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.imdbVotes}</p>
      <p class="subtitle">IMDB Votes</p>
    </article>
  `;
};





// ----------------------------------------------- //





// ----- SEARCH INPUT INCL. DROPDOWNLIST AND SEARCHED MOVIE HTML CODE DISPLAY ---------------- //

// ----- index.js ------ //

createAutoComplete({
	// restructured code responsible for "where to render an .autocomplete to"
	root: document.querySelector('.autocomplete'),
	// restructured code responsible for "how to show an individual item"
	renderOption(movie) {
		const imgSrc = movie.Poster === 'N/A' ? '' : movie.Poster;
		return `
            <img src="${imgSrc}" />
            ${movie.Title} (${movie.Year})
            `;
	},
	// restructured code responsible for "what to do when user clicks on one item showed in the dropdownList"
	onOptionSelect(movie) {
		onMovieSelect(movie);
	},
	// restructured code responsible for "fetching the selected from the dropdownList movie title into input"
	inputValue(movie) {
		return movie.Title;
	},
	// restructured code responsible for "API data fetch"
	async fetchData(searchTerm) {
		const response = await axios.get('http://www.omdbapi.com/', {
			params: {
				apikey: '16060606',
				s: searchTerm
			}
		});

		if (response.data.Error) {
			return [];
		}
		return response.data.Search;
	}
});

const onMovieSelect = async (movie) => {
	const response = await axios.get('http://www.omdbapi.com/', {
		params: {
			apikey: '16060606',
			i: movie.imdbID
		}
	});

	document.querySelector('#summary').innerHTML = movieTemplate(response.data);
};

const movieTemplate = (movieDetail) => {
	return `
    <article class="media">
      <figure class="media-left">
        <p class="image">
          <img src="${movieDetail.Poster}" />
        </p>
      </figure>
      <div class="media-content">
        <div class="content">
          <h1>${movieDetail.Title}</h1>
          <h4>${movieDetail.Genre}</h4>
          <p>${movieDetail.Plot}</p>
        </div>
      </div>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.Awards}</p>
      <p class="subtitle">Awards</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.BoxOffice}</p>
      <p class="subtitle">Box Office</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.Metascore}</p>
      <p class="subtitle">Metascore</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.imdbRating}</p>
      <p class="subtitle">IMDB Rating</p>
    </article>
    <article class="notification is-primary">
      <p class="title">${movieDetail.imdbVotes}</p>
      <p class="subtitle">IMDB Votes</p>
    </article>
  `;
};


// ----- autocomplete.js ------ //

// autocomplete.js file is dedicated to the refactored app components that will be refactored for the purpose of future use therefore they needs to be universal; to make it possible, every application specific elements will be passed to createAutoComplete variable via its arguments
const createAutoComplete = ({ root, renderOption, onOptionSelect, inputValue, fetchData }) => {
	root.innerHTML = `
    <label><b>Search</b></label>
    <input class="input" />
    <div class="dropdown">
        <div class="dropdown-menu">
        <div class="dropdown-content results"></div>
        </div>
    </div>
    `;

	const input = root.querySelector('input');
	const dropdown = root.querySelector('.dropdown');
	const resultsWrapper = root.querySelector('.results');

	const onInput = async (event) => {
		const items = await fetchData(event.target.value);
		if (!items.length) {
			dropdown.classList.remove('is-active');
			return;
		}

		resultsWrapper.innerHTML = '';

		dropdown.classList.add('is-active');
		for (let item of items) {
			const option = document.createElement('a');

			option.classList.add('dropdown-item');
			option.innerHTML = renderOption(item);
			option.addEventListener('click', () => {
				dropdown.classList.remove('is-active');
				input.value = inputValue(item);
				onOptionSelect(item);
			});

			resultsWrapper.appendChild(option);
		}
	};
	input.addEventListener('input', debounce(onInput, 500));

	document.addEventListener('click', (event) => {
		if (!root.contains(event.target)) {
			dropdown.classList.remove('is-active');
		}
	});
};



// ----------------------------------------------- //